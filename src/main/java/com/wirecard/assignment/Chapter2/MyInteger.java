package com.wirecard.assignment.Chapter2;

public class MyInteger {

    private int value;

    public  MyInteger(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isEven(){
        if(value%2==0){
            return true;
        }
        return false;
    }

    public boolean isOdd(){
        if(value%2!=0){
            return true;
        }
        return false;
    }

    public boolean isPrime(){
        for(int i=2;i<value;i++) {
            if(value%i==0)
                return false;
        }
        return true;
    }

    public boolean isEven(int value){
        if(value%2==0){
            return true;
        }
        return false;
    }

    public boolean isOdd(int value){
        if(value%2!=0){
            return true;
        }
        return false;
    }

    public boolean isPrime(int value){
        for(int i=2;i<value;i++) {
            if(value%i==0)
                return false;
        }
        return true;
    }

    public boolean isEven(MyInteger value){
        if(value.getValue() %2==0){
            return true;
        }
        return false;
    }

    public boolean isOdd(MyInteger value){
        if(value.getValue()%2!=0){
            return true;
        }
        return false;
    }

    public boolean isPrime(MyInteger value){
        for(int i=2;i<value.getValue();i++) {
            if(value.getValue()%i==0)
                return false;
        }
        return true;
    }

    public boolean equals(int value){
        return value == this.value?true:false;
    }

    public boolean equals(MyInteger value){
        return value.getValue() == this.value?true:false;
    }

    public static int parseInt(String value){

        int i=0;
        int result=0;
        int digit=0;
        boolean negative=false;

        if(value.length()>0){
            char firstChar = value.charAt(0);

            if(firstChar < '0'){
                if(firstChar =='-'){
                    negative=true;
                }

                i++;
            }

            while(i<value.length()){
                digit = Character.digit(value.charAt(i++),10);
                result *= 10;
                result -= digit;
            }

            return negative ? result : -result;
        }
            return 0;
    }

}
