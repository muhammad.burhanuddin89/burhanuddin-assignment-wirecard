package com.wirecard.assignment.Chapter2;

public class Fan{

  static final short SLOW = 1;
  static final short MEDIUM = 2;
  static final short FAST = 3;

  private int speed = SLOW;
  private boolean on = false;
  private double radius = 5;
  private String color = "blue";

  public Fan(){}

  public void setSpeed(int speed){this.speed = speed;}
  public int getSpeed(){ return speed;}

  public void setOn(boolean on){this.on=on;}
  public boolean getOn(){return on;}

  public void setRadius(double radius){this.radius=radius;}
  public double getRadius(){return radius;}

  public void setColor(String color){this.color=color;}
  public String getColor(){return color;}

  public String toString(){
    if(on){
      return "fan speed is "+getSpeed()+", fan color is "+getColor()
      +" fan radius is "+getRadius();
    }

    return "fan is Off";
  }

}
