package com.wirecard.assignment.Chapter2;

public class MyString {
    char globalChr;
    int counterMethod;

    int[] result;

    public MyString(String str){
        this.result = new int[str.length()];
    }

    public int[] count(String s){
        char a = (globalChr == '\u0000')?s.charAt(0):globalChr;
        result[counterMethod]=count(s,a);
        counterMethod++;
        if(counterMethod < s.length()){
            globalChr = s.charAt(counterMethod);
            count(s);
        }
        return result;
    }

    public int count(String str, char a){
        int countChar = 0;
        for(int i = 0;i<str.length(); i++){
            if(a==str.charAt(i)){
                countChar++;
            }
        }
        return countChar;
    }

    public static boolean isAnagram(String s1, String s2){
        boolean result = false;
        if(s1.length() == s2.length()){
            for(int i=0;i<s1.length();i++){
                result = s2.contains(Character.toString(s1.charAt(i)));
                result = s1.contains(Character.toString(s2.charAt(i)));
                if(!result)
                    break;
            }
            return result;
        }else if(s1.length() < s2.length()){
            for(int i=0;i<s1.length();i++){
                result = s2.contains(Character.toString(s1.charAt(i)));
                if(!result)
                    break;
            }
            return result;
        }else  if(s1.length() > s2.length()){
            for(int i=0;i<s2.length();i++){
                result = s1.contains(Character.toString(s2.charAt(i)));
                if(!result)
                    break;
            }
            return result;
        }

        return result;
    }
}
