package com.wirecard.assignment.Chapter1;

import java.util.*;

public class Test1 {
    static Scanner myObj = new Scanner(System.in);
    static int num1=0;
    // Method task number start
    public static void number1(){
        System.out.println("=========================");
        System.out.println("NUMBER 1");
        System.out.println("=========================");
        System.out.println("Enter Amount :");
        double investmentAmount = myObj.nextDouble();

        System.out.println("Enter Monthly Investment Rate :");
        double monthlyInvestmentRate = myObj.nextDouble()/100/12;


        System.out.println("Enter number of Year :");
        int numberOfYear = myObj.nextInt();

        double futureInvestmentValue = investmentAmount*Math.pow((1+monthlyInvestmentRate),(numberOfYear*12));
        System.out.println("Result :" +futureInvestmentValue);
        System.out.println("=========================");

    }

    public static void number2(){
        System.out.println("=========================");
        System.out.println("NUMBER 2");
        System.out.println("=========================");
        double interest = 0.0;

        System.out.println("Enter Balance :");
        double balance = myObj.nextDouble();

        System.out.println("Enter Annual Interest rate :");
        double annualInterestRate = myObj.nextDouble();

        interest = balance*(annualInterestRate/1200);


        System.out.println("Result : "+interest);
        System.out.println("=========================");

    }

    public static void number3(){
        System.out.println("=========================");
        System.out.println("NUMBER 3");

        System.out.println("Enter Number 1 :");
        num1 = myObj.nextInt();
        System.out.println("Enter Number 2 :");
        int num2 = myObj.nextInt();
        System.out.println("Enter Number 3 :");
        int num3 = myObj.nextInt();

        int[] numArray = {num1,num2,num3};
        Arrays.sort(numArray);
        String numberSortResult = "";

        for(int i=0;i<numArray.length;i++){
            numberSortResult = numberSortResult+numArray[i];
        }

        System.out.println("Sorting Result is :"+numberSortResult);
        System.out.println("=========================");

    }

    public static void number5(){
        System.out.println("=========================");
        System.out.println("NUMBER 3");
        System.out.println("Berapa counter ?");
        num1 = myObj.nextInt();
        System.out.println("Berapa Kilo ?");
        int num2 = myObj.nextInt();
        double pound = 2.2;

        for(int i=0; i<num1;i++){
            if(num2 == 0){
                continue;
            }
                System.out.println("Kilo :"+num2);
                System.out.println("pound :"+num2*pound);

            num2 =num2 + 2;
        }

        System.out.println("=========================");

    }

    public static void number6(){
        System.out.println("=========================");
        System.out.println("NUMBER 3");
        System.out.println("Number of Student");
        num1 = myObj.nextInt();
        double[] scoreList = new double[num1];

        for(int i=0; i<num1;i++){
            System.out.println("Student Name :");
             myObj.next();
            System.out.println("Student Score :");
            double score = myObj.nextDouble();
            scoreList[i]=score;
        }

        double highestScore = Arrays.stream(scoreList).max().getAsDouble();
        System.out.println("The higest score is :"+highestScore);


        System.out.println("=========================");

    }

    public static void number4(){
        System.out.println("=========================");
        System.out.println("NUMBER 4");

        System.out.println("Enter Number :");
        num1 = myObj.nextInt();

        if((num1%5==0)&&(num1%6==0)){
            System.out.println(num1+" is divisible by both 5 and 6");
        }else if((num1%5==0)||(num1%6==0)){
            System.out.println(num1+" is divisible by 5 or 6, but not both");
        }else{
            System.out.println(num1+"  is not divisible by either 5 or 6");
        }

        System.out.println("=========================");

    }

    public static void number7_1(){
        System.out.println("=========================");
        System.out.println("NUMBER 7.1");

        System.out.println("Number of count");
        num1 = myObj.nextInt();
        for(int i=1;i<=num1;i++){
            for(int j=1;j<=i;j++){
                System.out.print(j);
            }
            System.out.println("");
        }

        System.out.println("=========================");
    }
    public static void number7_2(){
        System.out.println("=========================");
        System.out.println("NUMBER 7.2");

        System.out.println("Number of count");
        num1 = myObj.nextInt();
        for(int i=num1;i>=1;i--){
            for(int j=1;j<=i;j++){
                System.out.print(j);
            }
            System.out.println("");
        }

        System.out.println("=========================");
    }
    public static void number7_3(){
        System.out.println("=========================");
        System.out.println("NUMBER 7.3");
        System.out.println("Number of count");
        num1 = myObj.nextInt();

        for(int i=1;i<=num1;i++){
            for(int j=num1;j>=i;j--){
                System.out.print(" ");
            }
            for(int j=1;j<=i;j++){
                System.out.print(j);
            }
            System.out.println("");
        }

        System.out.println("=========================");
    }
    public static void number7_4(){
        System.out.println("=========================");
        System.out.println("NUMBER 7.4");
      	System.out.println("Number of count");
      	num1 = myObj.nextInt();

      	for(int i=num1;i>=1;i--){
      		for(int j=num1;j>i;j--){
      			System.out.print(" ");
      		}

      		for(int k=1;k<=i;k++){
      			System.out.print(k);
      		}
      		System.out.println("");

      	}
        System.out.println("=========================");
    }

    public static void number8(){
        System.out.println("=========================");
        System.out.println("NUMBER 8");
        System.out.println("Number of count");
      	num1 = myObj.nextInt();
        double sumNUm = 0;
      	for(int i=1;i<=num1;i+=2){
          sumNUm = sumNUm + (double)i/(i+2);
          System.out.println("pecahan :"+(double)i/(i+2));
          System.out.println("sumnum temp :"+sumNUm);
        }

        System.out.println("sumnum Result :"+sumNUm);
        System.out.println("=========================");

    }

    public static void number10(){
      System.out.println("=========================");
      System.out.println("NUMBER 10");
      System.out.println("Input number :");
      num1 = myObj.nextInt();
      System.out.println("Result for sumDigit is :"+sumDigit(num1));
      System.out.println("=========================");
    }

    public static void number11(){
      System.out.println("=========================");
      System.out.println("NUMBER 11");
      System.out.println("Input number :");
      num1 = myObj.nextInt();
      System.out.println("Result for reverseDigit is :"+reverseDigit(num1));
      System.out.println("=========================");
    }

    public static void number12(){
      System.out.println("=========================");
      System.out.println("NUMBER 12");
      System.out.println("Input number :");
      long numLong = myObj.nextLong();

      System.out.println("Result for Ticket is :"+ticketNumber(numLong));
      System.out.println("=========================");
    }

    public static void number13(){

        System.out.println("=========================");
        System.out.println("NUMBER 1");
        System.out.println("=========================");
        System.out.println("Enter Amount :");
        double investmentAmount = myObj.nextDouble();

        System.out.println("Enter Monthly Investment Rate :");
        double monthlyInvestmentRate = myObj.nextDouble()/100/12;


        System.out.println("Enter number of Year :");
        int numberOfYear = myObj.nextInt();

        for(int i=1; i<=numberOfYear; i++){
            System.out.println("Year :"+i);
            System.out.println("Future Value :"+futureInvestmentValue(investmentAmount,monthlyInvestmentRate,i));
        }

        System.out.println("=========================");

    }

    public static void number14(){

        System.out.println("=========================");
        System.out.println("NUMBER 14");
        System.out.println("=========================");
        System.out.println("Input Number :");
        int inputNumber = myObj.nextInt();

        System.out.println("Result total is :"+sumEvenNumber(inputNumber));

        System.out.println("=========================");

    }

    public static void number15(){

        System.out.println("=========================");
        System.out.println("NUMBER 15");
        System.out.println("=========================");
        int resultSum=0;

        System.out.println("Input Counter :");
        int inputCounter = myObj.nextInt();

        for(int i=0;i<inputCounter;i++){
          System.out.println("Input Number index"+(i+1));
          int inputNumber = myObj.nextInt();

          resultSum = resultSum + inputNumber;
        }
        System.out.println("Result total is :"+resultSum);
        System.out.println("=========================");

    }

    public static void number16(){

        System.out.println("=========================");
        System.out.println("NUMBER 16");
        System.out.println("=========================");
        int resultSum=0;
        String numberResult="";

        System.out.println("Input Counter :");
        int inputCounter = myObj.nextInt();
        int[] arrayNumber = new int[inputCounter];

        for(int i=0;i<inputCounter;i++){
          System.out.println("Input Number index"+(i+1));
          int inputNumber = myObj.nextInt();
          arrayNumber[i] = inputNumber;
        }

        for(int a=arrayNumber.length-1;a>=0;a--){
            numberResult = numberResult + arrayNumber[a];
        }
        System.out.println("Result total is :"+numberResult);
        System.out.println("=========================");

    }

    public static void number17(){

        System.out.println("=========================");
        System.out.println("NUMBER 17");
        System.out.println("=========================");

        int[] arrayInt = new int[]{1, 2, 3, 4, 5, 6};
        double[] arrayDouble = new double[]{6.0, 4.4, 1.9, 2.9, 3.4, 3.5};

        System.out.println("Result average double param :"+average(arrayDouble));
        System.out.println("Result average int param is :"+average(arrayInt));

        System.out.println("=========================");

    }

    public static void number18(){

        System.out.println("=========================");
        System.out.println("NUMBER 18");
        System.out.println("=========================");

        int[] arrayInt = new int[]{1,2,4,5,10,100,2,-22};

        int min = Arrays.stream(arrayInt).min().getAsInt();

        System.out.println("minimum result is :"+min);
        System.out.println("=========================");

    }

    public static void number19(){

        System.out.println("=========================");
        System.out.println("NUMBER 19");
        System.out.println("=========================");

        System.out.println("Input side 1 :");
        double side1 = myObj.nextDouble();

        System.out.println("Input side 2 :");
        double side2 = myObj.nextDouble();

        System.out.println("Input side 3 :");
        double side3 = myObj.nextDouble();

        if(Triangle.isValid(side1,side2,side3)){
          System.out.println("Area result is :"+Triangle.area(side1,side2,side3));
        }

        System.out.println("=========================");

    }

    public static void number20(){

        System.out.println("=========================");
        System.out.println("NUMBER 20");
        System.out.println("=========================");

        int resultSumAll = 0;
        int[][] arrayInt = new int[][]{{1,2,4,5},{6,7,8,9},{10,11,12,13},{14,15,16,17}};

        for(int a=0;a<arrayInt.length;a++){

          for(int i=0;i<arrayInt.length;i++){
              resultSumAll = resultSumAll + arrayInt[a][i];
          }

        }

        System.out.println("Result All is :"+resultSumAll);
        System.out.println("=========================");

    }

    // Method task number end

//Method util start
    public static int sumDigit(long number){
      String digitNumString = number+"";
      String[] arrDigitNumber = digitNumString.split("");
      int result = 0;
      for(int i=0; i<arrDigitNumber.length;i++){
        result = result + Integer.parseInt(arrDigitNumber[i]);
      }
      return result;
    }

    public static long ticketNumber(long number){
      String numberString = number+"";

      char lastIndexChar = numberString.charAt(numberString.length()-1);

      String remainString = numberString.substring(0,numberString.length()-1);

      long numberTicket = Long.parseLong(remainString);

      long dividedNumTicket = numberTicket%7;

      System.out.println("lastIndexChar : "+Character.getNumericValue(lastIndexChar));
      System.out.println("dividedNumTicket : "+dividedNumTicket);
      if(dividedNumTicket == Character.getNumericValue(lastIndexChar)){
         System.out.println("Ticket is valid");
      }else{
        System.out.println("Ticket not valid");
      }

      return Long.parseLong(remainString);
    }


    public static int reverseDigit(long number){
      String digitNumString = number+"";
      String[] arrDigitNumber = digitNumString.split("");
      String result = "";
      int lengthData = arrDigitNumber.length-1;
      for(int i=lengthData; i>=0;i--){
        result = result + arrDigitNumber[i];
      }
      return Integer.parseInt(result);
    }

    public static double sumEvenNumber(int inputNumber){

      double result = 1;

      for(int a=2; a<=inputNumber;a+=2){
        result = result - (double)1/((2*a)-1) + (double)1/((2*a)+1);
      }

      return 4*result;
    }

    public static double futureInvestmentValue(double investmentAmount, double monthlyInvestmentRate, int numberOfYear){
      double futureInvestmentValue = investmentAmount*Math.pow((1+monthlyInvestmentRate),(numberOfYear*12));
      return futureInvestmentValue;
    }

    public static double average(int[] array){
      double sum = 0;
      for(int i=0;i<array.length;i++){
        sum=sum+(double)array[i];
      }

      return sum/array.length;
    }

    public static double average(double[] array){
      double sum = 0;
      for(int i=0;i<array.length;i++){
        sum=sum+array[i];
      }

      return sum/array.length;
    }

//Method util end

    public static void main(String[] args){
       // number1();
       // number2();
//        number3();
//        number4();
//        number5();
//        number6();
//        number7_1();
//        number7_2();
//        number7_3();
//        number7_4();
          // number8();
          // number10();
          // number11();
          // number12();
      // number13();
      // number14();
      // number15();
      // number16();
      // number17();
      // number18();
      // number19();
      number20();
    }


}
