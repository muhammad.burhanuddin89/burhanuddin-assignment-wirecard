package com.wirecard.assignment.Chapter1;

public class Triangle {

  public static boolean isValid(double side1, double side2, double side3){
    double sum=0;

    sum = side1 + side2;

    if(sum > side3){
      return true;
    }

    return false;
  }

  public static double area(double side1, double side2, double side3){
    double s=0;
    double multiplyS=0;

    s = (side1+side2+side3)/2;
    System.out.println("(Triangle) S result is :"+s);
    multiplyS = s*(s-side1)*(s-side2)*(s-side3);
    System.out.println("(Triangle) multiplyS result is :"+multiplyS);

    return Math.sqrt(multiplyS);

  }

}
